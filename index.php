<!DOCTYPE html>
<html>
  <head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"  media="screen,projection"/>
    <style>
      a {
        color: unset;
      }
      .material-icons {
          margin-right: 5px;
      }
    </style>
    <?php 
        include 'config.php';

        $subpath = ".";

        if (isset($_GET["subpath"])) {
            $subpath = $_GET["subpath"];
            $folderToScan = "./" . $folderToScan . "/" . $subpath;
        }


        $currentFolder = "./" . $subpath;
    ?>
  </head>

  <body>

    <nav class="blue-grey lighten-2">
        <div class="nav-wrapper">
          <div class="col s12 center">
            <?php
              if (!isset($_GET["subpath"])) 
                  echo "File Manager";
              else
                  foreach (explode("/",$currentFolder) as $key => $value) {
                    $back = "?subpath=";
                    foreach (explode("/",$currentFolder) as $key1 => $value1)
                      if ($key1 <= $key && $key1 != 0)
                        $back = $back . "/" . $value1;
                       
                    if ($value != "")
                      echo "<a href='$back' class='breadcrumb'>$value</a>";
                  } 
            ?>
          </div>
        </div>
    </nav>

    <ul class="collection">
      <?php
        $sortedData = array();
        foreach(scandir($folderToScan) as $file) 
          if($file != '.' && $file != '..')
            if (!is_dir("$folderToScan/$file"))
              array_push($sortedData, "<li class=\"collection-item\" style='display: flex;'><i class='material-icons'>insert_drive_file</i> <a href=\"$folderToScan/$file\">$file</a></li>"); // add to end
            else
              if ($subpath == ".")
                array_unshift($sortedData, "<li class=\"collection-item\" style='display: flex;'><i class='material-icons'>folder</i> <a href=\"?subpath=$file\"><b>$file</b></a></li>"); // add to beginning
              else
                array_unshift($sortedData, "<li class=\"collection-item\" style='display: flex;'><i class='material-icons'>folder</i> <a href=\"?subpath=$subpath/$file\"><b>$file</b></a></li>"); // add to beginning
        foreach ($sortedData as $out)
          echo $out;
      ?>
    </ul>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </body>
</html>